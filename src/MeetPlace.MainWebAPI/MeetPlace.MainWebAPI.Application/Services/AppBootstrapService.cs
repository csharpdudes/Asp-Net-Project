using MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;
using Microsoft.Extensions.Logging;

namespace MeetPlace.MainWebAPI.Application.Services;

public class AppBootstrapService : IAppBootstrapService
{
    private readonly ILogger<AppBootstrapService> _logger;
    private readonly IAppDbContextInitializer _appDbContextInitializer;

    public AppBootstrapService(IAppDbContextInitializer appDbContextInitializer,
        ILogger<AppBootstrapService> logger)
    {
        _appDbContextInitializer = appDbContextInitializer;
        _logger = logger;
    }

    public void InitializeApp()
    {
        _appDbContextInitializer.Initialise();
    }
}