namespace MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;

public interface ICurrentUserService
{
    string? Id { get; }
}