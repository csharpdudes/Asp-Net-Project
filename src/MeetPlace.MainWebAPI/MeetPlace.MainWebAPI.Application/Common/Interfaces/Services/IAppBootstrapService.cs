namespace MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;

public interface IAppBootstrapService
{
    void InitializeApp();
}