namespace MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;

public interface IAppDbContextInitializer
{
    void Initialise();
    void Seed();
    void ClearTables();
}