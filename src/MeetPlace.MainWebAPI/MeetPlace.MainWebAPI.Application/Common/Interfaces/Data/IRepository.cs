using MeetPlace.MainWebAPI.Domain.Common;

namespace MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;

public interface IRepository<T>
    where T : BaseEntity
{
    Task<IEnumerable<T>> GetAllAsync(CancellationToken token,
        bool asNoTracking = false);

    Task<T> GetByIdAsync(Guid id,
        CancellationToken token);

    Task<int> AddAsync(
        T entity,
        CancellationToken token);

    Task AddRangeAsync(
        List<T> entities,
        CancellationToken token);

    void Update(T entity);

    Task<bool> DeleteAsync(Guid id,
        CancellationToken token);

    Task<bool> DeleteRangeAsync(
        List<int> ids,
        CancellationToken token);

    Task SaveChangesAsync(CancellationToken token);
}