namespace MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;

public interface IMeetPlaceRepository : IRepository<Domain.Entities.MeetPlace>
{
}