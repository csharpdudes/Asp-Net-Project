﻿using MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;
using MeetPlace.MainWebAPI.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MeetPlace.MainWebAPI.Application;

public static class ConfigureServices
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddScoped<IAppBootstrapService, AppBootstrapService>();

        return services;
    }
}