using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MeetPlace.MainWebAPI.Infrastructure.Data.Configurations;

public class MeetPlaceConfig : IEntityTypeConfiguration<Domain.Entities.MeetPlace>
{
    public void Configure(EntityTypeBuilder<Domain.Entities.MeetPlace> builder)
    {
        builder.Property(e => e.Id).ValueGeneratedOnAdd();
        builder.Property(e => e.Name)
            .HasMaxLength(200)
            .IsRequired();
    }
}