using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace MeetPlace.MainWebAPI.Infrastructure.Data;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }
    
    public DbSet<Domain.Entities.MeetPlace> MeetPlaces => Set<Domain.Entities.MeetPlace>();
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        base.OnModelCreating(builder);
    }
}