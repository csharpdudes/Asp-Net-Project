using MeetPlace.MainWebAPI.Application.Common.Interfaces;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MeetPlace.MainWebAPI.Infrastructure.Data;

public class AppDbContextInitializer : IAppDbContextInitializer
{
    private readonly ILogger<AppDbContextInitializer> _logger;
    private readonly AppDbContext _dbContext;

    public AppDbContextInitializer(ILogger<AppDbContextInitializer> logger,
        AppDbContext context)
    {
        _logger = logger;
        _dbContext = context;
    }

    public void Initialise()
    {
        try
        {
            if (!_dbContext.Database.CanConnect())
            {
                _dbContext.Database.EnsureCreated();
            }

            _dbContext.Database.Migrate();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $"An error occurred while initialising the database.");
            throw;
        }
    }

    public void Seed()
    {
        throw new NotImplementedException();
    }

    public void ClearTables()
    {
        throw new NotImplementedException();
    }
}