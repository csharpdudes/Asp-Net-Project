using MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;

namespace MeetPlace.MainWebAPI.Infrastructure.Data.Repositories;

public class MeetPlaceRepository : EfRepository<Domain.Entities.MeetPlace>, IMeetPlaceRepository
{
    public MeetPlaceRepository(AppDbContext context) : base(context)
    {
    }
}