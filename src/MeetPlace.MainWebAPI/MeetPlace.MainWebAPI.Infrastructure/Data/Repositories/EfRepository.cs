using MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;
using MeetPlace.MainWebAPI.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace MeetPlace.MainWebAPI.Infrastructure.Data.Repositories;

public class EfRepository<T> : IRepository<T>
    where T : BaseEntity
{
    protected readonly AppDbContext Context;
    protected readonly DbSet<T> EntitySet;

    protected EfRepository(AppDbContext context)
    {
        Context = context;
        EntitySet = Context.Set<T>();
    }

    public virtual async Task<IEnumerable<T>> GetAllAsync(CancellationToken token,
        bool asNoTracking = false) =>
        await (asNoTracking
            ? EntitySet.AsNoTracking().ToListAsync(token)
            : EntitySet.ToListAsync(token));

    public virtual async Task<T> GetByIdAsync(Guid id,
        CancellationToken token) =>
        await EntitySet.FindAsync(id, token);

    public virtual async Task<int> AddAsync(
        T entity,
        CancellationToken token)
    {
        await EntitySet.AddAsync(entity, token);
        await Context.SaveChangesAsync(token);
        return entity.Id;
    }

    public virtual async Task AddRangeAsync(
        List<T> entities,
        CancellationToken token)
    {
        await EntitySet.AddRangeAsync(entities, token);
        await Context.SaveChangesAsync(token);
    }

    public virtual void Update(T entity)
    {
        throw new NotImplementedException();
    }

    public virtual async Task<bool> DeleteAsync(
        Guid id,
        CancellationToken token)
    {
        var entity = await EntitySet.FindAsync(id, token);
        if (entity == null) return false;

        EntitySet.Remove(entity);
        await Context.SaveChangesAsync(token);
        return true;
    }

    public virtual async Task<bool> DeleteRangeAsync(
        List<int> ids,
        CancellationToken token)
    {
        var entities = await EntitySet.Where(e => ids.Contains(e.Id)).ToListAsync(token);
        if (!entities.Any()) return false;

        EntitySet.RemoveRange(entities);
        await Context.SaveChangesAsync(token);
        return true;
    }

    public virtual async Task SaveChangesAsync(CancellationToken token) =>
        await Context.SaveChangesAsync(token);
}