﻿using MeetPlace.MainWebAPI.Application.Common.Interfaces;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Data;
using MeetPlace.MainWebAPI.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MeetPlace.MainWebAPI.Infrastructure;

public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        var connection = configuration.GetConnectionString("ConnectionDB");
           services.AddDbContext<AppDbContext>(o => o.UseNpgsql(connection));
           
        services.AddScoped<IAppDbContextInitializer, AppDbContextInitializer>();

        return services;
    }
}