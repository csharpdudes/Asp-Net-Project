namespace MeetPlace.MainWebAPI.Domain.Common;

public abstract class BaseEntity
{
    public int Id { get; set; }
}