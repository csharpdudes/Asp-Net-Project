using MeetPlace.MainWebAPI.Domain.Common;

namespace MeetPlace.MainWebAPI.Domain.Entities;

public class MeetPlace : BaseAuditableEntity
{
    public string Name { get; set; } = string.Empty;
}