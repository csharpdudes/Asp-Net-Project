using MeetPlace.MainWebAPI.Application.Common.Interfaces;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;
using MeetPlace.MainWebAPI.WebAPI.Services;

namespace MeetPlace.MainWebAPI.WebAPI;

public static class ConfigureServices
{
    public static IServiceCollection AddWebAPIServices(this IServiceCollection services)
    {
        services.AddScoped<ICurrentUserService, CurrentUserService>();
        
        services.AddHttpContextAccessor();
        
        return services;
    }
}