using Microsoft.AspNetCore.Mvc;

namespace MeetPlace.MainWebAPI.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class MeetPlaceController : ControllerBase
{
    /// <summary>
    /// Тестовый метод
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<string>> Get(CancellationToken token)
    {
        return Ok("It's work!");
    }
}