using System.Security.Claims;
using MeetPlace.MainWebAPI.Application.Common.Interfaces;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;

namespace MeetPlace.MainWebAPI.WebAPI.Services;

public class CurrentUserService : ICurrentUserService
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public CurrentUserService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public string? Id => _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
}