using MeetPlace.MainWebAPI.Application;
using MeetPlace.MainWebAPI.Application.Common.Interfaces.Services;
using MeetPlace.MainWebAPI.Infrastructure;
using MeetPlace.MainWebAPI.WebAPI;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddWebAPIServices();

builder.Services.AddControllers();
builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();

var appBootstrap = app.Services.GetRequiredService<IAppBootstrapService>();
appBootstrap?.InitializeApp();