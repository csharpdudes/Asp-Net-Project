import React, { FC } from 'react';
import s from './ShortEventBlock.module.sass';
import { shortEventType } from "../../store/app.types";

export interface shortEventProps {
  shortEvent: shortEventType;
}

export const ShortEventBlock: FC<shortEventProps> = ({ shortEvent }) => {
  return (
    <div className={s.shortEvent}>
      <div className={s.title}>{shortEvent.title}</div>
      <div className={s.date}>{shortEvent.dateStart}</div>
      <div className={s.date}>{shortEvent.dateEnd}</div>
      <div className={s.text}>{shortEvent.location}</div>
    </div>
  );
};
