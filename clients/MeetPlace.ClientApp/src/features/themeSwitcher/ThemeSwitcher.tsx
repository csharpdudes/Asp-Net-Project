import React, { FC } from 'react';
import cn from 'clsx';
import { Sun } from '@gravity-ui/icons';
import { Moon } from '@gravity-ui/icons';
import { useThemeContext, Theme } from '../../theming';
import s from './ThemeSwitcher.module.sass';

export type ThemeSwitcherProps = {
  className?: string;
};

const icons = {
  [Theme.light]: <Moon />,
  [Theme.dark]: <Sun />,
};

export const ThemeSwitcher: FC<ThemeSwitcherProps> = ({ className }) => {
  const { theme, toggleTheme } = useThemeContext();
  return (
    <button type="button" className={cn(s.root, className)} onClick={toggleTheme}>
      {icons[theme]}
    </button>
  );
};
