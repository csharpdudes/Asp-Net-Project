import React from 'react'
import {useNavigate} from 'react-router-dom'
import s from "./ExitButton.module.sass"
import { signOut } from '../../../../store/profileSlice'
import { useDispatch } from 'react-redux'
import { NavLink, NavLinkProps } from 'react-router-dom';
import cn from 'clsx';

export const getClassName: NavLinkProps['className'] = ({ isActive }) => cn(s.link, isActive && s.active);

const ExitButton = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch()
  function handleClick() {
    navigate("/signOut");
    dispatch(signOut());
  }

  return (
    <NavLink className={getClassName} onClick={handleClick} to="/signin">
      Выход
    </NavLink>
  )
}

export default ExitButton