import React from 'react'
import verticalLine from './verticalLine.svg'
import s from "./NoAuthorization.module.sass"
import { NavLink, NavLinkProps } from 'react-router-dom';
import cn from 'clsx';

export const getClassName: NavLinkProps['className'] = ({ isActive }) => cn(s.link, isActive && s.active);

const NoAuthorization = () => {

return (
    <div className={s.panel}>
      <NavLink className={getClassName} to="/signin">
        Войти
      </NavLink>
      <img className={s.panel__verticalLine} src={verticalLine} alt='verticalLine' />
      <NavLink className={getClassName} to="/signup">
        Зарегистрироваться
      </NavLink>
    </div>
  )}

export default NoAuthorization