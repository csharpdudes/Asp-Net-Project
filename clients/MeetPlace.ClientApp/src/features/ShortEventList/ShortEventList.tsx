import React, { FC, useEffect, useState } from 'react';
import s from './ShortEventList.module.sass';
import { ShortEventBlock } from '../ShortEventBlock/ShortEventBlock';
import { shortEventType } from 'src/store/app.types';
  
interface DefinitionProps {
    shortDefinition?: shortEventType[];
}
  
  export const ShortEventList: FC<DefinitionProps> = ({ shortDefinition = [] }) => {
    const [shortDefinitionProductList, setShortDefinitionProductList] = useState(shortDefinition);
  
    useEffect(() => {
      // Обновляем внутреннее состояние, когда меняется shortDefinitionProduct
      setShortDefinitionProductList(shortDefinition);
    }, [shortDefinition]);
  
    const productList = shortDefinitionProductList.map((item) => {
      const shortEvent: shortEventType = {
        title: item.title,
        dateStart: item.dateStart,
        dateEnd: item.dateEnd,
        location: item.location,
        id: item.id,
      };
      return (
        <div className={s.list__div} key={item.id} id={item.id.toString()}>
          <ShortEventBlock shortEvent={shortEvent} />
        </div>
      );
    });
  
    return <div className={s.list}>{productList}</div>;
  };
  