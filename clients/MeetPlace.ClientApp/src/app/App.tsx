import React, {useEffect} from 'react';
import { BrowserRouter } from 'react-router-dom';
import Layout from '../layout/Layout';
import { ThemeProvider } from '../theming';
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'src/store/store';
import { ThunkDispatch } from 'redux-thunk';
import { UnknownAction } from '@reduxjs/toolkit';
import { setProfile } from '../store/profileSlice';
import { Navigation } from './Navigation';

function App() {
  type AppDispatch = ThunkDispatch<void, any, UnknownAction>;
  const dispatch: AppDispatch = useDispatch();
  const token = useSelector<RootState, string>((state) => state.profileSlice.token);
  useEffect(()=>{
    const accessToken = localStorage.getItem('accessToken')
    if(accessToken){
      //////
      var name = "Anna V"
      var email = "sdsds@mail.ru"
      dispatch(setProfile({ email, name }))
      //////
    }
  },[dispatch, token])

  return (
    <div>
      <ThemeProvider>
        <BrowserRouter>
          <Layout>
            <Navigation />
          </Layout>
        </BrowserRouter>
      </ThemeProvider>
    </div>
  );
}

export default App;
