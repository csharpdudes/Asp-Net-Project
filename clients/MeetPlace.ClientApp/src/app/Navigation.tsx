import React from 'react';
import { Route, Routes } from 'react-router-dom';
import HomeScreen from 'src/screens/HomeScreen/Home';
import NotFoundScreen from 'src/screens/NotFoundScreen';
import RegistrationScreen from 'src/screens/RegistrationScreen';
import AuthorizationScreen from 'src/screens/AuthorizationScreen';

export const Navigation: React.FC = () => (
    <Routes>
      <Route path="/" element={<HomeScreen />} />
      <Route path="/signin" element={<AuthorizationScreen />} />
      <Route path="/signup" element={<RegistrationScreen />} />
      <Route path="*" element={<NotFoundScreen />} />
    </Routes>
);
