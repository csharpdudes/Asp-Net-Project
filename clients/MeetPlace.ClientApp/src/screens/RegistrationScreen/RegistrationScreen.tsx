import React, { FC } from 'react';
import { Page } from 'src/components/Page';
import s from './RegistrationScreen.sass';
import { LoginUserForm } from 'src/components/Forms/loginUserForm/LoginUserForm';

export const RegistrationScreen: FC = () => {
  return (
    <Page title="Регистрация" className={s.root}>
      <LoginUserForm registration={true} />
    </Page>
  );
};

export default RegistrationScreen;