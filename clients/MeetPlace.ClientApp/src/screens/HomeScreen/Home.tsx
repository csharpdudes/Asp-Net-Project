import React, { FC, useEffect } from 'react';
import { Page } from 'src/components/Page';
import s from './Home.module.sass';
import { shortEventType } from 'src/store/app.types';
import { ShortEventList } from 'src/features/ShortEventList/ShortEventList';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'src/store/store';
import { addEvents } from 'src/store/eventSlice';

export const Home: FC = () => {
  const dispatch = useDispatch();
  const eventList = useSelector((state: RootState) => state.eventSlice.data);

  return (
    <Page title=" " className={s.root}>
      {/* <div>Инструмент, для упрощения процесса планирования, организации и управления различными мероприятиями.</div>
      <div>
Мы выбрали тему управления мероприятиями, потому что видим в этом потенциал для 
улучшения событийной деятельности. Наш проект направлен на создание удобного инструмента, 
который объединяет людей, предоставляя возможность создавать и посещать уникальные события, делиться опытом и вдохновлять друг друга.
      </div> */}
      <div>
        <ShortEventList shortDefinition={eventList} />
      </div>
    </Page>
  );
};

export default Home;
