import React, { FC } from 'react';
import { Page } from 'src/components/Page';
import s from './AuthorizationScreen.sass';
import { LoginUserForm } from 'src/components/Forms/loginUserForm/LoginUserForm';

export const AuthorizationScreen: FC = () => {
  return (
    <Page title="Вход" className={s.root}>
      <LoginUserForm registration={false} />
    </Page>
  );
};

export default AuthorizationScreen;