import React, { FC } from 'react';
import s from './NotFoundScreen.module.sass';

export const NotFound: FC = () => <div className={s.root}>NotFound</div>;

export default NotFound;
