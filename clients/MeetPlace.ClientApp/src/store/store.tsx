import { configureStore } from '@reduxjs/toolkit';

import profileSlice from './profileSlice';
import messageSlice from './messageSlice';
import eventSlice from './eventSlice';

export const store = configureStore({
  reducer: {
    profileSlice,
    messageSlice,
    eventSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
