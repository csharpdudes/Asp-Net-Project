import { createSlice } from "@reduxjs/toolkit";
import { shortEventType } from "./app.types";

export interface StateProps {
  data: shortEventType[];
  loading: boolean;
  errors: string[];
}
const initialState: StateProps = {
  data: [{"title": "Событие 1",
  "dateStart": "22.01.2024",
  "dateEnd": "25.01.2024",
  "location": "Москва",
  "id": "sdsdss",},
  {"title": "Событие 2",
  "dateStart": "22.01.2024",
  "dateEnd": "25.01.2024",
  "location": "Москва",
  "id": "sdscvcvdss",},
  {"title": "Событие 3",
  "dateStart": "22.01.2024",
  "dateEnd": "25.01.2024",
  "location": "Москва",
  "id": "cvcv",},
  {"title": "Событие 4",
  "dateStart": "22.01.2024",
  "dateEnd": "25.01.2024",
  "location": "Москва",
  "id": "gfgfghh",}
],
  loading: false,
  errors: null,
};

const eventSlice = createSlice({
    name: 'events',
    initialState,
    reducers: {
      addEvents: (state = initialState, action) => {
        state.data = [...state.data, ...action.payload];
      },
      setErrors: (state = initialState, action) => {
        state.errors = action.payload;
      },
    },
  });
  
  export const { addEvents, setErrors } = eventSlice.actions;
  export default eventSlice.reducer;