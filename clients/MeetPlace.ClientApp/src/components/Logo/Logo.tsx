import React from 'react';
import s from './Logo.module.sass';

const Logo = () => {
  return (
    <div className={s.logo}>MEET PLACE</div>
)};

export default Logo;