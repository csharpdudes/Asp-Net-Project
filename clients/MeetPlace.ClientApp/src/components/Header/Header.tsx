import React, { useRef, useLayoutEffect } from 'react';
import s from './Header.module.sass';
import Logo from '../Logo/Logo';
import { ThemeSwitcher } from '../../features/themeSwitcher';
import NoAuthorization from '../../features/auth/NoAuthorization/NoAuthorization';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import { NavLink } from 'react-router-dom';
import YesAuthorization from '../../features/auth/YesAuthorization/YesAuthorization';

type HeaderProps = {
  children?: React.ReactNode;
};

const Header = ({ children }: HeaderProps) => {
  const stickyHeader = useRef(null);
  useLayoutEffect(() => {
    const header = document.getElementById('header');
    const fixedTop = stickyHeader.current.offsetTop;
    const stickyHeaderEvent = () => {
      if (window.scrollY > fixedTop) {
        header.classList.add(s.sticky_header);
      } else {
        header.classList.remove(s.sticky_header);
      }
    };
    window.addEventListener('scroll', stickyHeaderEvent);
  }, []);

  const isSingIn = useSelector<RootState, boolean>((state) => state.profileSlice.isSingIn);
  const Account = isSingIn ? YesAuthorization : NoAuthorization;

  return (
    <div className={s.header} id="header" ref={stickyHeader}>
      <NavLink to="/">
        <Logo />
      </NavLink>
      <ThemeSwitcher />
      <Account />
      {children}
    </div>
  );
};

export default Header;