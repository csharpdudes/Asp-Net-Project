import React, { FC } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import s from './LoginUserForm.module.sass';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';
import { UnknownAction } from '@reduxjs/toolkit';
import { SignInBody } from 'src/store/app.types';
import axios from 'axios';
import { setMessageErrors } from 'src/store/messageSlice';
import { setProfile } from 'src/store/profileSlice';

interface LoginUserFormProps {
  registration: boolean;
}
// eslint-disable-next-line react/prop-types
export const LoginUserForm: FC<LoginUserFormProps> = ({ registration }) => {
  const navigate = useNavigate();
  type AppDispatch = ThunkDispatch<SignInBody, any, UnknownAction>;
  const dispatch: AppDispatch = useDispatch();
  const {
    register,
    handleSubmit,
    reset,
    watch,
    formState: { errors },
  } = useForm<FormValues>({
    mode: 'onBlur',
  });
  interface FormValues {
    email: string;
    password: string;
    confirmPassword: string;
  }

  const clickSubmit: SubmitHandler<FormValues> = async (value: { email: any; password: any; }) => {
    const { email, password } = value;
    if (!registration){
      // axios.put(`${process.env.REACT_APP_BACK_URL}/signin`)
      // .then(res =>{ 

      // })
      // .catch(error => dispatch(
      //   setMessageErrors({
      //     caption: 'Ошибка при входе',
      //     errors: error,
      //     messageType: 'Error',
      //   })))
    }
    else {
      // SignUp
    }
    ////
    var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVC77"
    localStorage.setItem('accessToken', token);
    
    var name = "Anna V"
    dispatch(setProfile({ email, name }))
    ////

    navigate('/');
    reset();
  };

  const pass = watch('password');
  return (
    <>
      <form className={s.form} onSubmit={handleSubmit(clickSubmit)}>
        <div className={s.field}>
          <label className={s.label}>Email*</label>
          <input
            className={s.input_pass}
            placeholder="email"
            {...register('email', {
              required: 'Поле обязательно для заполнения',
            })}
          />
          <label className={s.error_label}>{errors.email?.message}</label>
        </div>
        <div className={s.field}>
          <label className={s.label}>Пароль*</label>
          <input
            className={s.input_pass}
            type="password"
            placeholder="Пароль"
            {...register('password', {
              required: 'Поле обязательно для заполнения',
              minLength: {
                value: 10,
                message: 'Пароль должен содержать не менее 10 символов',
              },
              pattern: {
                value: /^\w+$/,
                message: 'Пароль должен содержать латинские буквы, цифры и знаки _',
              },
            })}
          />
          <label className={s.error_label}>{errors.password?.message}</label>
        </div>
          {registration && (
            <div className={s.field}>
              <label className={s.label}>Повторите пароль*</label>
              <input
                className={s.input_pass}
                type="password"
                placeholder="Повторите пароль"
                {...register('confirmPassword', {
                  required: 'Поле обязательно для заполнения',
                  validate: (value: any) => value === pass || 'Пароли не совпадают',
                })}
              />

              <label className={s.error_label}>{errors.confirmPassword?.message}</label>
            </div>
          )}

          <button type="submit" className={s.button_send}>
            {registration ? 'Зарегистрироваться' : 'Войти'}
          </button>
      </form>
    </>
  );
};
