# MeetPlace

Проект созданный в рамках курса OTUS "C# ASP.NET Core разработчик".

## Технологический стэк

* [ASP.NET Core 8](https://docs.microsoft.com/en-us/aspnet/core/introduction-to-aspnet-core)
* [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
* [PostgreSQL](https://www.postgresql.org/)
* [React](https://react.dev/)

## Описание проекта

**MeetPlace** - сервис управления мероприятиями.

## Описание приложения

Проект имеет монолитную структуру и разделен на слои:

### Domain

Сущности, типы проекта

### Application

Содержит всю логику приложения. Зависит от уровня Domain. Определяет интерфейсы, которые реализует уровень инфраструктуры.

### Infrastructure

Зависит от уровня Application. Реализует интерфейсы уровня Application. Содержит классы для доступа к внешним ресурсам, таким как файловые системы, веб-сервисы, БД и т.д.

### WebAPI

Веб приложение на основе ASP.NET Core Web API. Зависит от уровня Application т.к. вызывает сервисы данного уровня. В классе Program вызываются инициализирующие методы слоев Application и Infrastructure.

### ClientApp
Клиентское приложение на React

## DataBase
БД проекта под управлением PostgreSQL. Чтобы развернуть экземпляр БД в docker контейнере выполните ряд команд:
* docker pull postgres
* docker run --name meetplace_pg -p 5432:5432 -e POSTGRES_PASSWORD=dBE8y@H8vIXwp -e POSTGRES_USER=mp_user -e POSTGRES_DB=meetplace -d postgres

## Команда проекта
* [Александр Коваль](https://gitlab.com/alkoval)
* [Александр Судаков](https://gitlab.com/a.s.sudakov)
* [Анна Высоких](https://gitlab.com/AnnaVysokikh)
* [Давид Биденко](https://gitlab.com/)
* [Эльдар Ганеев](https://gitlab.com/Ganeevez)

## License

MIT

**О, это бесплатно!**
